package com.thoughtworks.practice.item;

//Model the item Book
public class Book extends Item {
    public Book(String name, int ID, int quantity) {
        super(name, ID, quantity);
    }

    public Book(String name, int ID, int quantity, double cost) {
        super(name, ID, quantity, cost);
    }
}