package com.thoughtworks.practice.item;

import java.time.LocalDate;

//Model the customer request to be a member of the system
public class Membership extends Item {
    LocalDate expiry = LocalDate.now().plusMonths(1);
    private static final int MEMBERSHIP_COPIES = 1;

    public Membership(String name, int ID) {
        super(name, ID, MEMBERSHIP_COPIES);
    }

    public Membership(String name, int ID, double cost) {
        super(name, ID, MEMBERSHIP_COPIES, cost);
    }

    public LocalDate getExpiry() {
        return expiry;
    }
}