package com.thoughtworks.practice.service;

import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Membership;
import com.thoughtworks.practice.item.UpgradeMembership;
import com.thoughtworks.practice.stock.Inventory;

import java.util.ArrayList;
import java.util.List;

//Model the operations on Inventory
public class InventoryService {

    public boolean isItemPresent(Item item, Inventory inventory) {
        return inventory.getAvailableStock().contains(item);
    }

    public boolean isItemPresentInSufficientQuantity(Item orderedItem, Inventory inventory) {
        if (isItemPresent(orderedItem, inventory))
            return validateQuantity(orderedItem, inventory);
        return false;
    }

    private boolean validateQuantity(Item item, Inventory inventory) {
        return item.getQuantity() <= inventory.getItemFromStock(item).getQuantity();
    }

    public void updateRemainingQuantity(Item orderedItem, Inventory inventory) throws ItemOutOfStockException {
        if (!isItemPresentInSufficientQuantity(orderedItem, inventory))
            throw new ItemOutOfStockException();
        updateExistingItemInStock(orderedItem, inventory);
    }

    private void updateExistingItemInStock(Item orderedItem, Inventory inventory) {
        int index = inventory.getItemIndex(orderedItem);
        Item itemInStock = inventory.getItemFromStock(orderedItem);
        itemInStock.setQuantity(getUpdatedQuantity(orderedItem, itemInStock));
        inventory.updateExistingItemInStock(itemInStock, index);
    }

    private int getUpdatedQuantity(Item orderedItem, Item itemInStock) {
        return itemInStock.getQuantity() - orderedItem.getQuantity();
    }

    public List<Item> getMembershipRequestsAvailableInStock(Inventory inventory) {
        List<Item> itemsInStock = inventory.getAvailableStock();
        List<Item> membershipRequests = new ArrayList<>();
        for (Item item : itemsInStock) {
            if (item instanceof Membership && !(item instanceof UpgradeMembership))
                membershipRequests.add(item);
        }
        return membershipRequests;
    }
}