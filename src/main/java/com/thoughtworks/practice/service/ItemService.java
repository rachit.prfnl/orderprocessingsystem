package com.thoughtworks.practice.service;

import com.thoughtworks.practice.item.Item;

//Model the operations on an Item
public class ItemService {

    double getCostOfOneItem(Item item) {
        return item.getCost();
    }

    private int getNumberOfCopies(Item item) {
        return item.getQuantity();
    }

    public double getCostPerItem(Item item) {
        return getCostOfOneItem(item) * getNumberOfCopies(item);
    }
}