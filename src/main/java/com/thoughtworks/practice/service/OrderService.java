package com.thoughtworks.practice.service;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Membership;
import com.thoughtworks.practice.item.UpgradeMembership;

import java.util.ArrayList;
import java.util.List;

//Model the operations on an Order
public class OrderService {

    public List<Item> getMembershipsPerOrder(Order requestedOrder) {
        List<Item> orderedItems = requestedOrder.getOrderedItems();
        return getMembershipItems(orderedItems);
    }

    private List<Item> getMembershipItems(List<Item> orderedItems) {
        List<Item> itemsOfMembershipType = new ArrayList<>();
        for (Item item : orderedItems)
            if (isItemAMembershipRequest(item))
                itemsOfMembershipType.add(item);
        return itemsOfMembershipType;
    }

    private boolean isItemAMembershipRequest(Item item) {
        return item instanceof Membership && !(item instanceof UpgradeMembership);
    }

    public double getTotalCostPerOrder(Order requestedOrder) {
        List<Item> orderedItems = requestedOrder.getOrderedItems();
        double totalCost = 0;
        for (Item item : orderedItems)
            totalCost += new ItemService().getCostPerItem(item);
        return totalCost;
    }

    public int getTotalNumberOfCopiesPerOrder(Order requestedOrder) {
        List<Item> orderedItems = requestedOrder.getOrderedItems();
        int numberOfCopies = 0;
        for (Item item : orderedItems)
            numberOfCopies += item.getQuantity();
        return numberOfCopies;
    }

    public List<Item> getUpgradeMembershipsPerOrder(Order requestedOrder) {
        List<Item> orderedItems = requestedOrder.getOrderedItems();
        return getUpgradeMembershipItems(orderedItems);
    }

    private List<Item> getUpgradeMembershipItems(List<Item> orderedItems) {
        List<Item> itemsOfUpgradeMembershipType = new ArrayList<>();
        for (Item item : orderedItems)
            if (item instanceof UpgradeMembership)
                itemsOfUpgradeMembershipType.add(item);
        return itemsOfUpgradeMembershipType;
    }
}