package com.thoughtworks.practice;

import com.thoughtworks.practice.menu.Action;
import com.thoughtworks.practice.menu.Menu;
import com.thoughtworks.practice.stock.Inventory;
import com.thoughtworks.practice.ui.ConsoleIO;

//Start the processing of an order
public class Application {
    private static final String STOCK_FILE = "./src/main/java/com/thoughtworks/practice/stock/resource/Stock.txt";
    private Inventory inventory;
    private ConsoleIO consoleIO = new ConsoleIO();

    public Application() {
        this.inventory = new Inventory();
    }

    void start() {
        initialSetup();
        displayWelcomeMessage();
        startProcessing(inventory);
    }

    private void displayWelcomeMessage() {
        consoleIO.display("**************************************");
        consoleIO.display(getWelcomeMessage());
        consoleIO.display("**************************************");
    }

    String getWelcomeMessage() {
        return "  WELCOME TO ORDER PROCESSING SYSTEM";
    }

    public void startProcessing(Inventory inventory) {
        while (true) {
            displayMenu();
            int userChoice = getUserInputChoice();
            new Action().fulfill(userChoice, inventory);
        }
    }

    private int getUserInputChoice() {
        consoleIO.display("Please enter your choice: ");
        return consoleIO.takeIntegerInput();
    }

    private void displayMenu() {
        consoleIO.display("\nMENU OPTIONS:");
        for (int iterator = 0; iterator < Menu.values().length; iterator++)
            consoleIO.display(Menu.values()[iterator].getOption());
    }

    private void initialSetup() {
        inventory.load(STOCK_FILE);
    }
}