package com.thoughtworks.practice;

import com.thoughtworks.practice.EmailService.Driver;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.List;

public class ExportOrder {
    public void exportOrderToFile(List<String> orderedItems) {
        try {
            PrintStream originalOut = java.lang.System.out;
            PrintStream fileOut = new PrintStream("./invoice.txt");
            java.lang.System.setOut(fileOut);

            String format = "| %1$-18s| %2$-8s| %3$-13s| %4$-10s| %5$-18s|";
            java.lang.System.out.println("Please find your order details below:");
            java.lang.System.out.println("\n");
            java.lang.System.out.println("------------------------------------------------------------------------------");
            java.lang.System.out.println(String.format(format, "ITEM", "ID", "COST/ITEM", "QUANTITY", "TOTAL COST (INR)"));
            java.lang.System.out.println("------------------------------------------------------------------------------");
            for (String output : orderedItems) {
                String[] outputItem = output.split(" ");
                java.lang.System.out.println((String.format(format, outputItem[0], outputItem[1], outputItem[2], outputItem[3], outputItem[4])));
                java.lang.System.out.println("------------------------------------------------------------------------------");
            }

            Driver driver = new Driver();
            driver.sendEmailToUser();

            java.lang.System.setOut(originalOut);

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
    }
}
