package com.thoughtworks.practice.validator;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.OrderConfirmation;
import com.thoughtworks.practice.stock.Inventory;
import com.thoughtworks.practice.ui.ReasonUI;

//Analyse an order for finalization
public class OrderReview {

    public void analyse(Order processedOrder, String reason, Inventory inventory) {
        if (isOrderProcessedSuccessfully(processedOrder))
            new OrderConfirmation().confirm(processedOrder, inventory);
        else
            new ReasonUI().showReasonForFailure(reason);
    }

    boolean isOrderProcessedSuccessfully(Order processedOrder) {
        return processedOrder != null;
    }
}