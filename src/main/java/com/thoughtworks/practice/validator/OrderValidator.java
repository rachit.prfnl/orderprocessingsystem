package com.thoughtworks.practice.validator;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.service.InventoryService;
import com.thoughtworks.practice.service.OrderService;
import com.thoughtworks.practice.stock.Inventory;

import java.util.List;

//Validate an order
public class OrderValidator {

    public boolean validate(Order requestedOrder, Inventory inventory) {
        if (!validateMembership(requestedOrder)) return false;
        if (!validateUpgradeMembership(requestedOrder, inventory)) return false;
        return validateItemAvailability(requestedOrder, inventory);
    }

    private boolean validateUpgradeMembership(Order requestedOrder, Inventory inventory) {
        return isUpgradeMembershipCountValid(requestedOrder) && isMembershipRequestAlreadyExisting(requestedOrder, inventory);
    }

    private boolean isMembershipRequestAlreadyExisting(Order requestedOrder, Inventory inventory) {
        List<Item> upgrades = new OrderService().getUpgradeMembershipsPerOrder(requestedOrder);
        if (upgrades.isEmpty())
            return true;
        Item upgradeMembership = upgrades.get(0);
        List<Item> membershipRequestsInStock = new InventoryService().getMembershipRequestsAvailableInStock(inventory);
        for (Item membershipRequest : membershipRequestsInStock) {
            if (membershipRequest.getID() == upgradeMembership.getID() && membershipRequest.getQuantity() == 0)
                return true;
        }
        return false;
    }

    private boolean isUpgradeMembershipCountValid(Order requestedOrder) {
        int upgradeMembershipCount = new OrderService().getUpgradeMembershipsPerOrder(requestedOrder).size();
        return upgradeMembershipCount <= 1;
    }

    private boolean validateItemAvailability(Order requestedOrder, Inventory inventory) {
        List<Item> orderedItems = requestedOrder.getOrderedItems();
        for (Item item : orderedItems)
            if (isItemUnAvailable(item, inventory)) return false;
        return true;
    }

    private boolean validateMembership(Order requestedOrder) {
        return isMembershipCountValid(requestedOrder);
    }

    private boolean isItemUnAvailable(Item item, Inventory inventory) {
        return !new InventoryService().isItemPresentInSufficientQuantity(item, inventory);
    }

    private boolean isMembershipCountValid(Order requestedOrder) {
        int membershipCount = new OrderService().getMembershipsPerOrder(requestedOrder).size();
        return membershipCount <= 1;
    }
}