package com.thoughtworks.practice;

import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.manager.SystemManager;
import com.thoughtworks.practice.stock.Inventory;
import com.thoughtworks.practice.validator.OrderReview;

import java.util.List;

//Model the complete order processing system
public class System {
    private Cart cart = new Cart();
    private SystemManager systemManager = new SystemManager();

    public void placeOrder(Inventory inventory) {
        Order requestedOrder = getOrderFromUser();
        Order processedOrder = systemManager.getProcessedOrder(requestedOrder, inventory);
        String reasonForFailure = systemManager.getReasonsForUnsuccessfulProcessing(requestedOrder, inventory);
        new OrderReview().analyse(processedOrder, reasonForFailure, inventory);
    }

    private Order getOrderFromUser() {
        new User().addSelectedItemsToCart(cart);
        List<Item> itemsInCart = cart.getItemsInCart();
        return new Order(itemsInCart);
    }
}