package com.thoughtworks.practice.allocator;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.service.InventoryService;
import com.thoughtworks.practice.service.ItemOutOfStockException;
import com.thoughtworks.practice.stock.Inventory;

import java.util.List;

//Allocate the ordered items from inventory
public class StockAllocator {
    private InventoryService inventoryService = new InventoryService();

    public void allocate(Order requestedOrder, Inventory inventory) throws ItemOutOfStockException {
        List<Item> orderedItems = requestedOrder.getOrderedItems();
        for (Item item : orderedItems)
            inventoryService.updateRemainingQuantity(item, inventory);
    }
}