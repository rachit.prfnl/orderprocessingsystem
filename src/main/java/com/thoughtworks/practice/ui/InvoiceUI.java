package com.thoughtworks.practice.ui;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.service.OrderService;

import java.util.List;

//Display the Payment Slip
public class InvoiceUI {
    private ConsoleIO consoleIO = new ConsoleIO();

    public void showInvoice(Order acceptedOrder, List<String> orderDetails) {
        displayPaymentSlip(acceptedOrder, orderDetails);
    }

    private void displayPaymentSlip(Order acceptedOrder, List<String> outputList) {
        String format = "| %1$-18s| %2$-8s| %3$-13s| %4$-10s| %5$-18s|";
        displayTitleSlip(format);
        displayOrderItems(outputList, format);
        displayTotalAmount(acceptedOrder, format);
    }

    private void displayTitleSlip(String format) {
        consoleIO.display("\nYour Payment Slip:");
        consoleIO.display("------------------------------------------------------------------------------");
        consoleIO.display(String.format(format, "ITEM", "ID", "COST/ITEM", "QUANTITY", "TOTAL COST (INR)"));
        consoleIO.display("------------------------------------------------------------------------------");
    }

    private void displayOrderItems(List<String> outputList, String format) {
        for (String output : outputList) {
            String[] outputItem = output.split(" ");
            displayAllItems(outputItem, format);
        }
    }

    private void displayAllItems(String[] outputItem, String format) {
        consoleIO.display(String.format(format, outputItem[0], outputItem[1], outputItem[2], outputItem[3], outputItem[4]));
    }

    private void displayTotalAmount(Order acceptedOrder, String format) {
        consoleIO.display("------------------------------------------------------------------------------");
        consoleIO.display(String.format(format, "TOTAL", "", "", Integer.toString(new OrderService().getTotalNumberOfCopiesPerOrder(acceptedOrder)), Double.toString(new OrderService().getTotalCostPerOrder(acceptedOrder))));
        consoleIO.display("------------------------------------------------------------------------------");
    }
}