package com.thoughtworks.practice.ui;

import java.util.List;

//Model the input for order details
public class DetailInputUI {
    private ConsoleIO consoleIO = new ConsoleIO();

    public List<String> takeInput() {
        displayMessage("============", getOrderStartMessage());
        List<String> inputList = consoleIO.takeMultiLineInput();
        displayMessage(getOrderEndMessage(), "============");
        return inputList;
    }

    private void displayMessage(String message, String anotherMessage) {
        consoleIO.display(message);
        consoleIO.display(anotherMessage);
    }

    String getOrderStartMessage() {
        return "ORDER START \n(ITEM ID QUANTITY)";
    }

    String getOrderEndMessage() {
        return "ORDER END";
    }
}