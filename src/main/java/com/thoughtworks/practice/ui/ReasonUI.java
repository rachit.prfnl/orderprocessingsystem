package com.thoughtworks.practice.ui;

//Display the Reason for UnSuccessful Processing
public class ReasonUI {
    private ConsoleIO consoleIO = new ConsoleIO();

    public void showReasonForFailure(String reason) {
        consoleIO.display(String.format("\nCan't process the order as - %1$s", reason));
    }
}
