package com.thoughtworks.practice.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//Model the console input - output
public class ConsoleIO {
    private Scanner scanner = new Scanner(System.in);

    public void display(String message) {
        System.out.println(message);
    }

    List<String> takeMultiLineInput() {
        List<String> inputOrder = new ArrayList<>();
        do {
            inputOrder.add(takeStringInput().toUpperCase());
        } while (!isInputComplete());
        return inputOrder;
    }

    private boolean isInputComplete() {
        display("Do you want to add more items?(y/n)");
        char addMoreItems = scanner.nextLine().charAt(0);
        return addMoreItems == 'n' || addMoreItems == 'N';
    }

    private String takeStringInput() {
        return scanner.nextLine();
    }

    public boolean isInvoiceRequested() {
        display("\nDo you want to view Payment Slip?(y/n)");
        char invoiceRequested = scanner.nextLine().toLowerCase().charAt(0);
        return invoiceRequested == 'y';
    }

    public int takeIntegerInput() {
        return scanner.nextInt();
    }
}