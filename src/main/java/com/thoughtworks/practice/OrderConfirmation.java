package com.thoughtworks.practice;

import com.thoughtworks.practice.generator.DetailGenerator;
import com.thoughtworks.practice.generator.InvoiceGenerator;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.parser.StringParser;
import com.thoughtworks.practice.parser.invoice.InvoiceStringParser;
import com.thoughtworks.practice.stock.Inventory;
import com.thoughtworks.practice.ui.ConsoleIO;

import java.util.List;

//Finalize the Accepted Order
public class OrderConfirmation {

    public void confirm(Order processedOrder, Inventory inventory) {
        displayOrderSummary(processedOrder);
        SendEmailToCustomer(processedOrder);
        if (isUserRequestingInvoice()){
            displayInvoice(processedOrder);
        }
        new Application().startProcessing(inventory);
    }

    private void SendEmailToCustomer(Order processedOrder) {
        List<Item> processedItems = processedOrder.getOrderedItems();
        List<String> parsedString = new InvoiceStringParser().parseStringList(processedItems);
        ExportOrder exportOrder = new ExportOrder();
        exportOrder.exportOrderToFile(parsedString);
    }

    private boolean isUserRequestingInvoice() {
        return new ConsoleIO().isInvoiceRequested();
    }

    private void displayInvoice(Order processedOrder) {
        new InvoiceGenerator().generateInvoice(processedOrder);
    }

    private void displayOrderSummary(Order processedOrder) {
        new DetailGenerator().generateOrderDetails(processedOrder);
    }
}