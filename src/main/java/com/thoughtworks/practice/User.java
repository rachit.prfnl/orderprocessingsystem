package com.thoughtworks.practice;

import com.thoughtworks.practice.ui.DetailInputUI;

import java.util.List;

//Fetch the user input and add it to the cart
public class User {

    void addSelectedItemsToCart(Cart cart) {
        List<String> selectedItems = new DetailInputUI().takeInput();
        cart.addItems(selectedItems);
    }
}