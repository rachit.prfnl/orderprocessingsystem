package com.thoughtworks.practice.parser.invoice;

import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.parser.StringParser;
import com.thoughtworks.practice.service.ItemService;

//Parse the list of string associated with Invoice
public class InvoiceStringParser extends StringParser {

    public String getStringFromItem(Item item) {
        return item.getName() + " " + item.getID() + " " + item.getCost() + " " + item.getQuantity() + " " + new ItemService().getCostPerItem(item);
    }
}