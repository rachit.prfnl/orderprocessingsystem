package com.thoughtworks.practice.manager;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.processor.OrderProcessor;
import com.thoughtworks.practice.stock.Inventory;

//Fetch the processed order
public class SystemManager {

    public Order getProcessedOrder(Order requestedOrder, Inventory inventory) {
        return new OrderProcessor().process(requestedOrder, inventory);
    }

    public String getReasonsForUnsuccessfulProcessing(Order requestedOrder, Inventory inventory) {
        return new OrderProcessor().getReasons(requestedOrder, inventory);
    }
}