package com.thoughtworks.practice.stock;

import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.parser.details.DetailItemParser;

import java.util.ArrayList;
import java.util.List;

//Model the list of items in stock
public class Inventory {
    private List<Item> availableStock;

    public Inventory() {
        availableStock = new ArrayList<>();
    }

    public void load(String filePath) {
        List<String> stock = new FileReader().getFileContent(filePath);
        availableStock = new DetailItemParser().parseItemList(stock);
    }

    public List<Item> getAvailableStock() {
        return availableStock;
    }

    public int getItemIndex(Item item) {
        return getAvailableStock().indexOf(item);
    }

    public Item getItemFromStock(Item item) {
        return getAvailableStock().get(getItemIndex(item));
    }

    public void updateExistingItemInStock(Item item, int index) {
        getAvailableStock().set(index, item);
    }
}