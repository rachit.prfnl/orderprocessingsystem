package com.thoughtworks.practice;

import com.thoughtworks.practice.item.Item;

import java.util.List;

//Model the list of items in an order
public class Order {
    private List<Item> orderedItems;

    public Order(List<Item> orderedItems) {
        this.orderedItems = orderedItems;
    }

    public List<Item> getOrderedItems() {
        return orderedItems;
    }
}
