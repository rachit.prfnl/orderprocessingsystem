package com.thoughtworks.practice.EmailService;


import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.Properties;

public class SendEmail {
    void sendMail(EmailService emailService) {
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", emailService.getHost());
        prop.put("mail.smtp.port", emailService.getPort());
        prop.put("mail.smtp.ssl.trust", emailService.getHost());

        Session session = Session.getInstance(prop, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emailService.getUsername(),emailService.getPassword());
            }
        });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("rachit.prfnl@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("skapoor1997@gmail.com"));
            message.setSubject("DarkCart Order ID "+"1987654327");

            String msg;
            InputMessage inputMessage = new InputMessage();
            msg = inputMessage.getMessage();

            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(msg, "text/html");

            MimeBodyPart attachmentBodyPart = new MimeBodyPart();
            attachmentBodyPart.attachFile(new File(inputMessage.getAttachmentPath()));

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);
            multipart.addBodyPart(attachmentBodyPart);

            message.setContent(multipart);

            Transport.send(message);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
