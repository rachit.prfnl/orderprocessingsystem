package com.thoughtworks.practice.EmailService;

public class InputMessage {
    private String message="Hello," + "\n" +"Kindly find the invoice in the attachment below.";

    private String attachmentPath = "/Users/rachit.sharma/Downloads/ops/invoice.txt";

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public String getMessage() {
        return message;
    }
}
