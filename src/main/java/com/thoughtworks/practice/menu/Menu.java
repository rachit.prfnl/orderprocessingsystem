package com.thoughtworks.practice.menu;

//Model the available Menu Options
public enum Menu {
    PLACE_ORDER("Place Order") {
    },
    EXIT("EXIT");

    private final String option;

    Menu(String option) {
        this.option = "Enter " + (ordinal() + 1) + " to " + option;
    }

    public String getOption() {
        return option;
    }
}