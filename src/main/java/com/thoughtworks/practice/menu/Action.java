package com.thoughtworks.practice.menu;

import com.thoughtworks.practice.Application;
import com.thoughtworks.practice.System;
import com.thoughtworks.practice.stock.Inventory;
import com.thoughtworks.practice.ui.ConsoleIO;

import static java.lang.System.exit;

//Fulfill the user choice
public class Action {
    public void fulfill(int userChoice, Inventory inventory) {
        switch (userChoice) {
            case 1:
                new System().placeOrder(inventory);
                break;
            case 2:
                exit(0);
            default:
                new ConsoleIO().display("Invalid Choice");
                new Application().startProcessing(inventory);
        }
    }
}