package com.thoughtworks.practice.processor;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.allocator.StockAllocator;
import com.thoughtworks.practice.reason.InsufficientQuantityException;
import com.thoughtworks.practice.reason.ItemNotFoundException;
import com.thoughtworks.practice.reason.Reason;
import com.thoughtworks.practice.service.ItemOutOfStockException;
import com.thoughtworks.practice.stock.Inventory;
import com.thoughtworks.practice.validator.OrderValidator;

//Process the given order
public class OrderProcessor {
    private OrderValidator orderValidator = new OrderValidator();

    public Order process(Order requestedOrder, Inventory inventory) {
        if (orderValidator.validate(requestedOrder, inventory))
            return getAllocatedStock(requestedOrder, inventory);
        return null;
    }

    private Order getAllocatedStock(Order requestedOrder, Inventory inventory) {
        try {
            new StockAllocator().allocate(requestedOrder, inventory);
        } catch (ItemOutOfStockException e) {
            return null;
        }
        return requestedOrder;
    }

    public String getReasons(Order requestedOrder, Inventory inventory) {
        Reason reason = new Reason();
        try {
            reason.find(requestedOrder, inventory);
        } catch (ItemNotFoundException e) {
            return e.getItemNotFoundExceptionMessage();
        } catch (InsufficientQuantityException e) {
            return e.getInsufficientQuantityExceptionMessage();
        }
        return "Requested Order is Invalid";
    }
}