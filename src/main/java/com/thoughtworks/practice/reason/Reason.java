package com.thoughtworks.practice.reason;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.service.InventoryService;
import com.thoughtworks.practice.stock.Inventory;

import java.util.List;

//Find the reasons for Un-Successful Processing of an Order
public class Reason {

    public void find(Order requestedOrdered, Inventory inventory) throws ItemNotFoundException, InsufficientQuantityException {
        List<Item> orderedItems = requestedOrdered.getOrderedItems();
        for (Item item : orderedItems)
            if (isItemNotPresentInStock(item, inventory))
                throw new ItemNotFoundException();
            else if (isItemQuantityInsufficient(inventory, item))
                throw new InsufficientQuantityException();
    }

    private boolean isItemNotPresentInStock(Item item, Inventory inventory) {
        return !new InventoryService().isItemPresent(item, inventory);
    }

    private boolean isItemQuantityInsufficient(Inventory inventory, Item item) {
        return !new InventoryService().isItemPresentInSufficientQuantity(item, inventory);
    }
}