package com.thoughtworks.practice.generator;

import com.thoughtworks.practice.ExportOrder;
import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.parser.invoice.InvoiceStringParser;
import com.thoughtworks.practice.ui.InvoiceUI;

import java.util.List;

//Generates the invoice
public class InvoiceGenerator {

    public void generateInvoice(Order processedOrder) {
        List<Item> processedItems = processedOrder.getOrderedItems();
        List<String> parsedString = new InvoiceStringParser().parseStringList(processedItems);
        new InvoiceUI().showInvoice(processedOrder, parsedString);

    }
}