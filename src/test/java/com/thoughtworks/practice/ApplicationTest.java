package com.thoughtworks.practice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ApplicationTest {

    @Test
    void shouldGiveWelcomeMessage() {
        Application application = new Application();
        String expected = "  WELCOME TO ORDER PROCESSING SYSTEM";
        assertEquals(expected, application.getWelcomeMessage());
    }
}