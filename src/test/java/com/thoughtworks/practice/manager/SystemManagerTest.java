package com.thoughtworks.practice.manager;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import com.thoughtworks.practice.stock.Inventory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SystemManagerTest {
    private SystemManager systemManager;
    private Inventory inventory;

    @BeforeEach
    void setup() {
        systemManager = new SystemManager();
        inventory = new Inventory();
        inventory.load("./src/main/java/com/thoughtworks/practice/stock/resource/Stock.txt");
    }

    @Test
    void shouldGiveTheProcessedOrder() {
        List<Item> orderedItems = Arrays.asList(new Book("BOOK", 1, 2), new Video("VIDEO", 1));
        Order order = new Order(orderedItems);
        assertEquals(order, systemManager.getProcessedOrder(order, inventory));
    }
}