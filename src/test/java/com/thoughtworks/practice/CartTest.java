package com.thoughtworks.practice;

import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CartTest {
    private Cart cart;

    @BeforeEach
    void setup() {
        cart = new Cart();
    }

    @Test
    void shouldGiveItemsPresentInCart() {
        List<String> selectedItems = Arrays.asList("BOOK 1 2", "VIDEO 1");
        cart.addItems(selectedItems);
        List<Item> expected = Arrays.asList(new Book("BOOK", 1, 2), new Video("VIDEO", 1));
        assertEquals(expected, cart.getItemsInCart());
    }
}