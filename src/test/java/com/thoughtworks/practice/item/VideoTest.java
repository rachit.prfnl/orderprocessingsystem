package com.thoughtworks.practice.item;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class VideoTest {

    @Test
    void shouldGiveQuantityOfVideo() {
        Item video = new Video("VIDEO", 1);
        assertEquals(1, video.getQuantity());
    }
}