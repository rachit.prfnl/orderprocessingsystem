package com.thoughtworks.practice.processor;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import com.thoughtworks.practice.stock.Inventory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class OrderProcessorTest {
    private OrderProcessor orderProcessor;
    private Inventory inventory;

    @BeforeEach
    void setup() {
        orderProcessor = new OrderProcessor();
        inventory = new Inventory();
        inventory.load("./src/main/java/com/thoughtworks/practice/stock/resource/Stock.txt");
    }

    @Test
    void shouldProcessedTheRequestedOrder() {
        List<Item> orderedItems = Arrays.asList(new Book("BOOK", 1, 2), new Video("VIDEO", 2));
        Order order = new Order(orderedItems);
        assertEquals(order, orderProcessor.process(order, inventory));
    }

    @Test
    void shouldBeNullWhenOrderCannotBeProcessed() {
        List<Item> orderedItems = Arrays.asList(new Book("BOOK", 100, 2), new Video("VIDEO", 2));
        Order order = new Order(orderedItems);
        assertNull(orderProcessor.process(order, inventory));
    }

    @Test
    void shouldGiveReasonWhenBookIsNotFoundInInventory() {
        List<Item> orderedItems = Collections.singletonList(new Book("BOOK", 100, 2));
        Order order = new Order(orderedItems);
        String expected = "We do not sell the requested item.";
        assertEquals(expected, orderProcessor.getReasons(order, inventory));
    }

    @Test
    void shouldGiveReasonWhenVideoIsNotFoundInInventory() {
        List<Item> orderedItems = Collections.singletonList(new Video("VIDEO", 100));
        Order order = new Order(orderedItems);
        String expected = "We do not sell the requested item.";
        assertEquals(expected, orderProcessor.getReasons(order, inventory));
    }

    @Test
    void shouldGiveMessageWhenNoReasonIsFound() {
        List<Item> orderedItems = Collections.singletonList(new Video("VIDEO", 1));
        Order order = new Order(orderedItems);
        String expected = "Requested Order is Invalid";
        assertEquals(expected, orderProcessor.getReasons(order, inventory));
    }

    @Test
    void shouldGiveReasonWhenBookIsNotPresentInSufficientQuantity() {
        List<Item> orderedItems = Collections.singletonList(new Book("BOOK", 1, 100));
        Order order = new Order(orderedItems);
        String expected = "The requested item is not available in stock.";
        assertEquals(expected, orderProcessor.getReasons(order, inventory));
    }
}