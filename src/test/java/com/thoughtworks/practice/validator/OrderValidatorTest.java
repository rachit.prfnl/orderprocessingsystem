package com.thoughtworks.practice.validator;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.item.*;
import com.thoughtworks.practice.stock.Inventory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class OrderValidatorTest {
    private OrderValidator orderValidator;
    private Inventory inventory;

    @BeforeEach
    void setup() {
        orderValidator = new OrderValidator();
        inventory = new Inventory();
        inventory.load("./src/main/java/com/thoughtworks/practice/stock/resource/Stock.txt");
    }

    @Test
    void shouldBeTrueWhenGivenOrderIsValid() {
        Item book1 = new Book("BOOK", 1, 2);
        Item video1 = new Video("VIDEO", 2);
        Item book2 = new Book("BOOK", 2, 3);
        Order requestedOrder = new Order(Arrays.asList(book1, book2, video1));
        assertTrue(orderValidator.validate(requestedOrder, inventory));
    }

    @Test
    void shouldBeFalseWhenGivenOrderIsNotValid() {
        Item book1 = new Book("BOOK", 1, 2);
        Item video1 = new Video("VIDEO", 2);
        Item book2 = new Book("BOOK", 200, 3);
        Order requestedOrder = new Order(Arrays.asList(book1, book2, video1));
        assertFalse(orderValidator.validate(requestedOrder, inventory));
    }

    @Test
    void shouldBeTrueWhenMembershipCountDoesNotExceed1() {
        Item item1 = new Book("BOOK", 1, 2);
        Item item2 = new Membership("MEMBERSHIP", 101);
        Order requestedOrder = new Order(Arrays.asList(item1, item2));
        assertTrue(orderValidator.validate(requestedOrder, inventory));
    }

    @Test
    void shouldBeFalseWhenMembershipCountExceeds1() {
        Item item1 = new Membership("MEMBERSHIP", 101);
        Item item2 = new Membership("MEMBERSHIP", 102);
        Item item3 = new Book("BOOK", 1, 2);
        Order requestedOrder = new Order(Arrays.asList(item1, item2, item3));
        assertFalse(orderValidator.validate(requestedOrder, inventory));
    }

    @Test
    void shouldBeTrueWhenUpgradeMembershipCountDoesNotExceed1() {
        Item item1 = new Book("BOOK", 1, 2);
        Item item2 = new UpgradeMembership("UPGRADE_MEMBERSHIP", 101);
        Order requestedOrder = new Order(Arrays.asList(item1, item2));
        assertTrue(orderValidator.validate(requestedOrder, inventory));
    }

    @Test
    void shouldBeFalseWhenUpgradeMembershipCountExceeds1() {
        Item item1 = new UpgradeMembership("UPGRADE_MEMBERSHIP", 101);
        Item item2 = new UpgradeMembership("UPGRADE_MEMBERSHIP", 102);
        Item item3 = new Book("BOOK", 1, 2);
        Order requestedOrder = new Order(Arrays.asList(item1, item2, item3));
        assertFalse(orderValidator.validate(requestedOrder, inventory));
    }
}