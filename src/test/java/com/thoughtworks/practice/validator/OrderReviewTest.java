package com.thoughtworks.practice.validator;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.manager.SystemManager;
import com.thoughtworks.practice.stock.Inventory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class OrderReviewTest {
    private OrderReview orderReview;
    private Inventory inventory;

    @BeforeEach
    void setup() {
        orderReview = new OrderReview();
        inventory = new Inventory();
        inventory.load("./src/main/java/com/thoughtworks/practice/stock/resource/Stock.txt");
    }

    @Test
    void shouldBeTrueWhenOrderIsSuccessfullyProcessed() {
        Item book = new Book("BOOK", 1, 2);
        Order requestedOrder = new Order(Collections.singletonList(book));
        Order processedOrder = new SystemManager().getProcessedOrder(requestedOrder, inventory);
        assertTrue(orderReview.isOrderProcessedSuccessfully(processedOrder));
    }

    @Test
    void shouldBeFalseWhenOrderIsNotSuccessfullyProcessed() {
        Item book = new Book("BOOK", 100, 2);
        Order requestedOrder = new Order(Collections.singletonList(book));
        Order processedOrder = new SystemManager().getProcessedOrder(requestedOrder, inventory);
        assertFalse(orderReview.isOrderProcessedSuccessfully(processedOrder));
    }
}