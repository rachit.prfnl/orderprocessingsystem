package com.thoughtworks.practice.parser.invoice;

import com.thoughtworks.practice.item.*;
import com.thoughtworks.practice.parser.ItemParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InvoiceItemParserTest {
    private ItemParser itemParser;

    @BeforeEach
    void setup() {
        itemParser = new InvoiceItemParser();
    }

    @Test
    void shouldParseItemBook() {
        String[] stockDetails = {"BOOK", "1", "2"};
        assertEquals(new Book("BOOK", 1, 2, 100), (itemParser.getItemFromString(stockDetails)));
    }

    @Test
    void shouldParseItemVideo() {
        String[] stockDetails = {"VIDEO", "1"};
        assertEquals(new Video("VIDEO", 1, 50), (itemParser.getItemFromString(stockDetails)));
    }

    @Test
    void shouldGiveItemsInStock() {
        List<String> stockList = Arrays.asList("BOOK 1 2", "VIDEO 1");
        List<Item> expected = Arrays.asList(new Book("BOOK", 1, 2, 100), new Video("VIDEO", 1, 50));
        assertEquals(expected, itemParser.parseItemList(stockList));
    }

    @Test
    void shouldParseItemMembership() {
        String[] stockDetails = {"MEMBERSHIP", "101", "200"};
        assertEquals(new Membership("MEMBERSHIP", 101, 200), (itemParser.getItemFromString(stockDetails)));
    }

    @Test
    void shouldParseItemUpgradeMembership() {
        String[] stockDetails = {"UPGRADE_MEMBERSHIP", "101", "50"};
        assertEquals(new UpgradeMembership("UPGRADE_MEMBERSHIP", 101, 50), (itemParser.getItemFromString(stockDetails)));
    }
}