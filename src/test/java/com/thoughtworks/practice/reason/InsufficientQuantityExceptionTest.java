package com.thoughtworks.practice.reason;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InsufficientQuantityExceptionTest {
    private InsufficientQuantityException exception;

    @BeforeEach
    void setup() {
        exception = new InsufficientQuantityException();
    }

    @Test
    void shouldGiveExceptionMessageWhenItemIsInsufficientInQuantity() {
        assertEquals("The requested item is not available in stock.", exception.getInsufficientQuantityExceptionMessage());
    }
}