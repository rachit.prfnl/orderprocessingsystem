package com.thoughtworks.practice.reason;

import com.thoughtworks.practice.Order;
import com.thoughtworks.practice.item.Book;
import com.thoughtworks.practice.item.Item;
import com.thoughtworks.practice.item.Video;
import com.thoughtworks.practice.stock.Inventory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ReasonTest {
    private Reason reason;
    private Inventory inventory;

    @BeforeEach
    void setup() {
        reason = new Reason();
        inventory = new Inventory();
        inventory.load("./src/main/java/com/thoughtworks/practice/stock/resource/Stock.txt");
    }

    @Test
    void shouldThrowExceptionWhenItemNotFoundInStock() {
        Item book1 = new Book("BOOK", 1, 2);
        Item video1 = new Video("VIDEO", 2);
        Item book2 = new Book("BOOK", 200, 3);
        Order order = new Order(Arrays.asList(book1, book2, video1));
        assertThrows(ItemNotFoundException.class, () -> reason.find(order, inventory));
    }

    @Test
    void shouldNotThrowExceptionWhenItemFoundInStock() {
        Item book1 = new Book("BOOK", 1, 2);
        Item video1 = new Video("VIDEO", 2);
        Item book2 = new Book("BOOK", 2, 3);
        Order order = new Order(Arrays.asList(book1, book2, video1));
        assertDoesNotThrow(() -> reason.find(order, inventory));
    }

    @Test
    void shouldThrowExceptionWhenItemIsPresentInInsufficientQuantity() {
        List<Item> orderedItems = Collections.singletonList(new Book("BOOK", 1, 20));
        Order order = new Order(orderedItems);
        assertThrows(InsufficientQuantityException.class, () -> reason.find(order, inventory));
    }

    @Test
    void shouldNotThrowExceptionWhenItemIsPresentInSufficientQuantity() {
        List<Item> orderedItems = Collections.singletonList(new Book("BOOK", 1, 2));
        Order order = new Order(orderedItems);
        assertDoesNotThrow(() -> reason.find(order, inventory));
    }
}